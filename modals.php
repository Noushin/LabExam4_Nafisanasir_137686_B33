<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Picture Upload Modal</h2>
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">click for  Modal</button>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Model Header</h4>
                    <h5>Description</h5>

                    <input type="text" name="description" size="=100px" action="upload.php">
                    <h3>Date Format</h3>
                    <input type="date" name="date">
                    <form action="Carousel.php" method="post" enctype="multipart/form-data">
                        Select image to Upload:
                        <input type="file" name="fileToUpload" id="fileToUpload">
                        <input type="submit" value="Submit" class="btn btn-success" name="submit">
                    </form>

                </div>
            </div>

        </div>
    </div>

</div>

</body>
</html>